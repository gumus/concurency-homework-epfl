package handout;

import java.io.IOException;

//////////////////////////////////////////
//		EPFL - CONCURRENCY - CS206		//
//					HW1					//
//	Student-1: KIVAN� KAMAY - 253799	//
//	Student-2: OR�UN G�M��	- XXXXXX	//
//////////////////////////////////////////

public class SobelMain 
{
	// Implement this !!!
	static int[][] sobel(int[][] img, int threshold, int nOfThreads) throws InterruptedException
	{		
		int imageWidth = img.length;
		int imageHeight= img[0].length;
		
		// Output Image
		int[][] outImg = new int[imageWidth][imageHeight];
		
		// Threads
		Thread[] threads = new Thread[nOfThreads];
		
		// Jobs for threads
		RunnableJob[] jobs = new RunnableJob[nOfThreads];		
		
		int quotient = imageHeight / nOfThreads;
		int remainder = imageHeight % nOfThreads;
		boolean divides = false;
		
		if(remainder == 0)
			divides = true;
		
		int count=0;
		
		for(int i=0; i < nOfThreads; i++)
		{
			jobs[i] = new RunnableJob(img, outImg, 0, count, threshold, quotient);
			threads[i] = new Thread(jobs[i]);
			
			count += quotient;
		}
		
		for(int i=0; i < nOfThreads; i++)
		{
			threads[i].start();
		}
		
		for(int i=0; i < nOfThreads; i++)
		{
			threads[i].join();
		}
		
		if(!divides)
		{
			for(int i=0; i < remainder; i++)
			{
				jobs[i] = new RunnableJob(img, outImg, 0, count, threshold, 1);
				threads[i] = new Thread(jobs[i]);
				
				count++;
			}
			
			for(int i=0; i < remainder; i++)
			{
				threads[i].start();
			}
			
			for(int i=0; i < remainder; i++)
			{
				threads[i].join();
			}
		}
		
		/*
		//Direct implementation of the pseudo-code, without threads
		
		// Kernels for the vertical and horizontal directions
		final int[][] Ky = {
				{-1, -2, -1},
				{0, 0, 0},
				{1, 2, 1}
		};
		
		final int[][] Kx = {
				{-1, 0, 1},
				{-2, 0, 2},
				{-1, 0, 1}
		};
		
		for(int y=0; y < imageHeight; y++)
		{
			for(int x=0; x < imageWidth; x++)
			{
				int convResultX = 0; 
				int convResultY = 0;
				
				for(int i=0; i < 3; i++)
				{
					for(int j=0; j < 3; j++)
					{
						int clampedX = (x+i-1 < 0) ? 0 : ( (x+i-1 > imageWidth - 1) ? imageWidth - 1 : x+i-1);
						int clampedY = (y+j-1 < 0) ? 0 : ( (y+j-1 > imageHeight -1) ? imageHeight - 1 : y+j-1);
						
						convResultX += img[clampedX][clampedY] * Kx[i][j];
						convResultY += img[clampedX][clampedY] * Ky[i][j];
					}
				}
				
				if( (Math.abs(convResultX) + Math.abs(convResultY)) > threshold )
					outImg[x][y] = 255;
				else
					outImg[x][y] = 0;
			}
		}*/
		
		return outImg;
	}
	
	public static void main(String[] args) throws IOException, InterruptedException
	{
		int[][] image;
		String filename = args[0];
		int nOfThreads= Integer.parseInt(args[1]);
		int threshold= Integer.parseInt(args[2]);

		image = SimpleImage.getImage(filename);
		
		StopWatch sw = new StopWatch();
		sw.start();
		int[][] imgOut = SobelMain.sobel(image,threshold,nOfThreads);
		sw.stop();
		System.out.println(sw.getStrElapsedTime());

		 SimpleImage.writeImage(imgOut, filename + ".out");
	}

}

