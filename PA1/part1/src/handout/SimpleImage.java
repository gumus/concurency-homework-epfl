package handout;

import java.awt.color.ColorSpace;
import java.awt.image.BufferedImage;
import java.awt.image.ColorConvertOp;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class SimpleImage {

	/**
	 * Get a pixel from the coordinates (x,y)
	 * @param x X coordinate
	 * @param y Y coordinate
	 * @return Pixel value, from 0 to 255
	 */
	private static int rgbToGrayscale(int rgb){
		// all channels are the same, get the lower one
		int r = (rgb>>16)&0xff;
		int g = (rgb>>8)&0xff;
		int b = rgb&0xff;	
		return (r+g+b)/3;	
	}

	/**
	 * Read an image from file
	 * @param filename Name of the image file
	 * @return Initialized image object
	 * @throws IOException if the file does not exists or is not an image
	 */
	static int[][] getImage(String filename) throws IOException {
		BufferedImage image = 
				ImageIO.read(new File(filename));
		
		ColorSpace cs = ColorSpace.getInstance(ColorSpace.CS_GRAY);  
		ColorConvertOp op = new ColorConvertOp(cs, null);  
		BufferedImage grayImage = op.filter(image, null); 	
		
		int imgHeight = grayImage.getHeight();
		int imgWidth = grayImage.getWidth();
		int[][] imgMatrix = new int[imgHeight][imgWidth];		
		for(int y=0; y<imgHeight; y++){
			for(int x=0; x<imgWidth; x++){
				imgMatrix[y][x] = rgbToGrayscale(grayImage.getRGB(x, y));
			}			
		}
		
		return imgMatrix;
	}

	/**
	 * Write a pixel to the coordinate (x,y)
	 * @param x X coordinate
	 * @param y Y coordinate
	 * @param pixel Pixel value, from 0 to 255, will be truncated
	 */
	static private int grayscaleToRgb(int grayscale){
		int truncatedPixel = Math.abs(grayscale) & 0xFF;
		int rgb = 255<<24 | truncatedPixel<<16 | truncatedPixel<<8 | truncatedPixel;
		return rgb;
	}
	
	/**
	 * Write an image to file
	 * @param image Image to be written
	 * @param filename Name of the file to be written, will be overritten if necessary
	 * @throws IOException If the new file cannot be created
	 */
	static void writeImage(int[][] image, String filename) throws IOException {
		int imgHeight = image.length;
		int imgWidth = image[0].length;
		
		BufferedImage newImage = new BufferedImage(imgWidth, imgHeight,
                BufferedImage.TYPE_BYTE_GRAY);
		for(int y=0; y<imgHeight; y++){
			for(int x=0; x<imgWidth; x++){
				newImage.setRGB(x, y, grayscaleToRgb(image[y][x]));
			}			
		}
		
	    File outputfile = new File(filename);
	    ImageIO.write(newImage, "png", outputfile);

	}
	
	
}
