package handout;

public class RunnableJob implements Runnable
{
	int imageWidth, imageHeight;
	int start_x, start_y;
	int threshold;
	int[][] img, outImg;
	int amount;
	
	// Kernels for the vertical and horizontal directions
	final int[][] Ky = {
			{-1, -2, -1},
			{0, 0, 0},
			{1, 2, 1}
	};
	
	final int[][] Kx = {
			{-1, 0, 1},
			{-2, 0, 2},
			{-1, 0, 1}
	};
	
	public RunnableJob(int[][] img, int[][] outImg, int start_x, int start_y, int threshold, int amount)
	{
		imageWidth = img.length;
		imageHeight = img[0].length;
		
		this.img = img;
		this.outImg = outImg;
		this.start_x = start_x;
		this.start_y = start_y;
		this.threshold = threshold;
		this.amount = amount;
	}
	
	public void run()
	{
		for(int y = start_y; y < (amount + start_y); y++)
		{
			for(int x = start_x; x < imageWidth; x++)
			{
				int convResultX = 0; 
				int convResultY = 0;
				
				for(int i=0; i < 3; i++)
				{
					for(int j=0; j < 3; j++)
					{
						int clampedX = (x+i-1 < 0) ? 0 : ( (x+i-1 > imageWidth - 1) ? imageWidth - 1 : x+i-1);
						int clampedY = (y+j-1 < 0) ? 0 : ( (y+j-1 > imageHeight -1) ? imageHeight - 1 : y+j-1);
						
						convResultX += img[clampedX][clampedY] * Kx[i][j];
						convResultY += img[clampedX][clampedY] * Ky[i][j];
					}
				}
				
				if( (Math.abs(convResultX) + Math.abs(convResultY)) > threshold )
					outImg[x][y] = 255;
				else
					outImg[x][y] = 0;
			}
		}
	}
}
