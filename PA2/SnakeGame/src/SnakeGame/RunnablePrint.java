package SnakeGame;

import java.util.concurrent.locks.ReentrantLock;

// This class basically defines the job for printing
// the game field for every 300 ms.

public class RunnablePrint implements Runnable 
{
	private GameField gf;
	private ReentrantLock lock;
	
	public RunnablePrint(GameField gf, ReentrantLock lock)
	{
		this.gf = gf;
		this.lock = lock;
	}
	
	@Override
	public void run() 
	{
		while(gf.getLeftSnakes())
		{			
			try 
			{
				Thread.sleep(300);
			} 
			catch (InterruptedException e) 
			{
				e.printStackTrace();
			}
			
			// Lock here
			lock.lock();
			
			for(int row=0; row < gf.getGSize(); row++)
			{
				for(int col=0; col < gf.getGSize(); col++)
				{
					if(gf.getField()[row][col] < 0)
						System.out.print("*" + " ");
					else
						System.out.print(gf.getField()[row][col] + " ");
				}
				
				System.out.print("\n");
			}
			System.out.print("\n");
			
			// Unlock here
			lock.unlock();
		}		
	}
}
