package SnakeGame;

// A class for representing snakes' parts:
// Each part has a location on the game field

public class SnakePart 
{
	private int loc_x, loc_y;
	
	public SnakePart(int x, int y)
	{
		loc_x = x;
		loc_y = y;
	}
	
	public void setLocations(int x, int y)
	{
		loc_x = x;
		loc_y = y;
	}
	
	public int getLocationX()
	{
		return loc_x; 
	}
	
	public int getLocationY()
	{
		return loc_y;
	}
}
