package SnakeGame;

import java.util.Random;
import java.util.concurrent.locks.ReentrantLock;

// A class for the game field. It holds most of the basic properties for the game such as
// the game field or list of the snakes on the field. 

public class GameField 
{	
	private ReentrantLock lock;
	private static int[][] field;
	private int gSize, nSnakes, snakeSize;
	private Snake[] snakeList;
	private Thread[] threads;
	private int nSteps;
	private boolean isThereAnySnake = false;		
	
	public GameField(int gSize, int nSnakes, int snakeSize, int nSteps, ReentrantLock lock)
	{
		this.gSize = (gSize);
		this.nSnakes = nSnakes;
		this.snakeSize = snakeSize;
		this.nSteps = nSteps;
		this.lock = lock;
		
		field = new int[gSize][gSize];
		snakeList = new Snake[nSnakes];
		
		// The elements in the field are initialized to "-1"s at the beginning.
		for(int row=0; row < gSize; row++)
		{
			for(int col=0; col < gSize; col++)
			{
				getField()[row][col] = -1;
			}
		}
		
		// Threads are created.
		threads = new Thread[nSnakes];
		
		initialize();
	}
	
	// Initializes snake positions at the beginning.
	private void initialize()
	{
		Random rand = new Random();
		
		for(int i=0; i < nSnakes; i++)
		{
			int head_y;
			
			do
			{
				head_y = rand.nextInt(getGSize());
			}while(checkOccupancy(head_y));
			
			snakeList[i] = new Snake(i, nSteps ,snakeSize, getField(), getGSize(), head_y, lock); 
			isThereAnySnake = true;
			
			threads[i] = new Thread(snakeList[i]);			
		}
		
	}
	
	// Method for starting the threads.
	public void start()
	{		
		for(int i=0; i < nSnakes; i++)
		{
			threads[i].start();
		}
	}
	
	// Method for stopping the threads.
	public void stop() throws InterruptedException
	{
		for(int i=0; i < nSnakes; i++)
		{
			threads[i].join();
		}
		
		isThereAnySnake = false;
	}
	
	// Checks if a column is occupied by a snake. 
	public boolean checkOccupancy(int y)
	{
		for(int i=0; i < getGSize(); i++)
		{
			if(getField()[i][y] >= 0)
				return true;
		}
		
		return false;
	}
	
	// Prints the log.
	public void printSummary()
	{
		System.out.println("*** LOG ***");
		
		for(int i=0; i < nSnakes; i++)
		{
			System.out.println(snakeList[i].toString());
		}
	}
	
	public int getGSize() 
	{
		return gSize;
	}
	
	public int[][] getField() 
	{
		return field;
	}
	
	public boolean getLeftSnakes()
	{
		return isThereAnySnake;
	}
}
