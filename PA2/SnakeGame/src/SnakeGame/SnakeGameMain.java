package SnakeGame;

import java.util.concurrent.locks.ReentrantLock;

//////////////////////////////////////////
//		EPFL - CONCURRENCY - CS206		//
//					HW2					//
//	Student-1: KIVAN� KAMAY - 253799	//
//	Student-2: OR�UN G�M��	- 228199	//
//////////////////////////////////////////

// The project involves 5 classes:
// 		- SnakeGameMain
//		- GameField
//		- RunnablePrint
//		- Snake
//		- SnakePart

// This is the driver (main) class of the project.

public class SnakeGameMain 
{
	// Lock for the threads
	private static ReentrantLock lock;	
	
	public static void main(String[] args) throws InterruptedException
	{
		if(args.length == 4)
		{			
			/* Received arguments:
			 * gSize =  The size of a side of the grid (The grid will have gSize x gSize cells).
			 * nSnakes = The number of snakes to be in the grid.
			 * snakeSize = The size of the snakes.
			 * nSteps = Minimum number of steps by each snake before they stop. 
			 */
			
			int gSize = Integer.parseInt(args[0]);
			int nSnakes = Integer.parseInt(args[1]);
			int snakeSize = Integer.parseInt(args[2]);
			int nSteps = Integer.parseInt(args[3]);
			
			lock = new ReentrantLock();
			 
			GameField field = new GameField(gSize, nSnakes, snakeSize,nSteps,lock);	
			RunnablePrint print = new RunnablePrint(field,lock);
			Thread printJob = new Thread(print);
			
			field.start(); 
			printJob.start();
			
			try
			{
				field.stop();
			}
			catch (InterruptedException e) 
			{				
				e.printStackTrace();
			}
			finally
			{
				printJob.join();
			}
			
			field.printSummary();
		}
		else
			System.out.println("Please provide exactly 4 arguments!");
	}
}
