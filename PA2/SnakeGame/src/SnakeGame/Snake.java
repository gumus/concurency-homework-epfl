package SnakeGame;

import java.util.Random;
import java.util.concurrent.locks.ReentrantLock;

// A class for simply representing the snake objects
// on the game field.

public class Snake implements Runnable 
{
	// Directions for the snakes
	private static final int NORTH = 0;
	private static final int EAST = 1;
	private static final int SOUTH = 2;
	private static final int WEST = 3;

	private ReentrantLock lock; // The reentrant lock.
	private int id;	// ID of the snake, it is represented as its id on the field.
	private int nSteps; // # of steps for the snake before it dies.
	private SnakePart[] locations; // Parts of the snake, they have the information for their location on the field. locations[0] is the head of the snake.
	private int[][] field; // Game field.
	private int gSize; // Size of the game field, it is a square matrix.
	private int size; // Size of the snake.
	private int direction; // Direction that the snake is heading.
	private boolean dead; // Condition for whether the snake is alive or not.
	private String summary; // Summary of the movements of the snake.
	private Random rand; // Random number generator.
	
	public Snake(int id, int nSteps ,int size, int[][] field, int gSize, int head_y, ReentrantLock lock)
	{
		this.nSteps = nSteps;
		this.field = field;
		this.gSize = gSize;
		this.id = id;
		this.size = size;		
		this.lock = lock;
		
		rand = new Random();
		
		dead = false; 
		direction = 0; // Snake is heading NORTH at the beginning.
		summary = "";
		
		locations = new SnakePart[size];
		
		for(int i=0; i < size; i++)
		{
			locations[i] = new SnakePart(-100, -100);
		}
		
		// Initializes the snake on the field.
		setInitialLocations(1, head_y);
	}
	
	// Run method for the Snake thread.
	public void run()
	{
		try 
		{
			this.move();
		} 
		catch (InterruptedException e) 
		{
			e.printStackTrace();
		}
	}
	
	// Puts snake onto the game field.
	private void setInitialLocations(int head_x, int head_y)
	{		
		for(int row=head_x, i=0; i < size; row++, i++)
		{
			locations[i].setLocations(row, head_y);
			field[row][head_y] = id;
		}
	}
	
	// Movement method for the snake. It randomly chooses a direction and moves there if;
	//		- It is still alive.
	//		- The point that it chose is not occupied.
	// On the other hand, the snake dies if;
	//		- It touches itself or another snake.
	//
	// P.S: The snake wraps up to the other side of the game field if it moves through the edges.
	private void move() throws InterruptedException
	{		
		int next_x = 0;
		int next_y = 0;
		
		while(!dead)
		{
			Thread.sleep(100);
			
			int newDir;
			
			// Generates a random "direction". If the direction is invalid (i.e. 180 degrees backwards 
			// from the current direction), it continues generating. 
			do{
				newDir = rand.nextInt(4);
			} while( Math.abs(newDir - direction) == 2);
			
			direction = newDir;
			
			int x = locations[0].getLocationX();
			int y = locations[0].getLocationY();
			next_x = x;
			next_y = y;
			
			switch (direction)
			{
				case NORTH:									
					if(x == 0)
						if(field[gSize-1][y] < 0)
							next_x = gSize-1;
						else
							dead = true;
					else
						if(field[x - 1][y] < 0)
							next_x = x-1;
						else
							dead = true;
				break;
				
				case EAST:								
					if(y == gSize-1)
						if(field[x][0] < 0)
							next_y = 0;
						else
							dead = true;
					else
						if(field[x][y+1] < 0)
							next_y = y+1;
						else
							dead = true;
				break;
				
				case SOUTH:									
					if(x == gSize-1)
						if(field[0][y] < 0)
							next_x = 0;
						else
							dead = true;
					else
						if(field[x+1][y] < 0)
							next_x = x+1;
						else
							dead = true;
				break;
				
				case WEST:						
					if(y == 0)
						if(field[x][gSize-1] < 0)
							next_y = gSize-1;
						else
							dead = true;
					else
						if(field[x][y-1] < 0)
							next_y = y-1;
						else
							dead = true;
				break;
			}
			
			addMovementSummary(x, y, next_x, next_y, System.currentTimeMillis());
			
			for(int i=0; i < locations.length; i++)
			{
				int cur_x = locations[i].getLocationX();
				int cur_y = locations[i].getLocationY();
				
				locations[i].setLocations(next_x, next_y);
				
				lock.lock(); // Lock here
				
				field[next_x][next_y] = id;
				
				lock.unlock(); // Unlock here
				
				next_x = cur_x;
				next_y = cur_y;
			}
			
			field[next_x][next_y] = -1; // The field is set back to not occupied.
			
			if(--nSteps == 0) 
				dead = true;
		}
		
		addDeadSummary(next_x, next_y, System.currentTimeMillis());
	}
	
	// Adds movement summary to previous summary
	private void addMovementSummary(int oldposx, int oldposy, int newposx, int newposy, long time)
	{
		summary += "Snake " + id + " moved from: (" + oldposx + "," + oldposy + ") to (" + newposx + "," + newposy + ") at " + time + "\n";
	}
	
	// Adds dead summary to previous summary
	private void addDeadSummary(int posx, int posy, long time)
	{
		summary += "Snake " + id + " died at: (" + posx + "," + posy + ") at " + time + "\n";
	}
	
	// toString method
	public String toString()
	{
		return summary;
	}
	
	// Returns snake's condition
	public boolean getCondition()
	{
		return dead;
	}
}